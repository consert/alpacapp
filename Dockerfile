# BASE_IMAGE: 
# python3.8-slim for cpu
# nvcr.io/nvidia/cuda:12.1.1-runtime-ubuntu20.04 for gpu (to use cublas)
# ARG BASE_IMAGE=python:3.8-slim
ARG BASE_IMAGE=nvcr.io/nvidia/cuda:12.1.1-cudnn8-devel-ubuntu20.04
FROM $BASE_IMAGE

ARG BASE_IMAGE=nvcr.io/nvidia/cuda:12.1.1-cudnn8-devel-ubuntu20.04
ENV BASE_IMAGE=$BASE_IMAGE

# Install system dependencies
ENV DEBIAN_FRONTEND noninteractive
RUN bash -c 'if ! [ "${BASE_IMAGE}" = "python:3.8-slim" ]; then \
    apt-get update && apt-get -y --no-install-recommends install \
        python3.8-dev python3.8-venv python3-pip; \
    fi'
RUN apt-get update && apt-get -y --no-install-recommends install \
    build-essential \
    ca-certificates \
    curl \
    git \
    git-lfs \
    cmake \
    libopenblas-dev && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Add user
ARG GROUP_ID
ENV GROUP_ID=${GROUP_ID:-1000}
RUN addgroup --system --gid ${GROUP_ID} user
ARG USER_ID
ENV USER_ID=${USER_ID:-1000}
RUN adduser --disabled-password --gecos '' --shell /bin/bash --uid ${USER_ID} --gid ${GROUP_ID} user
RUN mkdir -p /home/user/app /home/user/data /home/user/.local/bin && \
    echo 'PATH=/home/user/.local/bin:$PATH' >> /home/user/.bashrc && \
    chown -R user:user /home/user

ENV PATH=/home/user/.local/bin:$PATH
ENV TZ=UTC

# Install python dependencies
# triton needs numpy and zmq to be in /usr/local ?
#   RuntimeError: Python environment for python backend is missing required packages. 
#   Ensure that you have numpy, zmq installed in the /usr/local environment. 
#   Installed modules _distutils_hack, pip, pkg_resources, setuptools, wheel.
#   Missing modules numpy, zmq.
RUN pip install --upgrade pip wheel setuptools numpy zmq

USER user
COPY --chown=user:user requirements/requirements.txt /tmp/requirements.txt

# cuBLAs for gpu, openblas for cpu
RUN bash -c 'python3 -m pip install --user -r /tmp/requirements.txt && \
        python3 -m pip install --user cmake scikit-build && \
        cd /tmp && \
        git clone --recurse-submodules https://github.com/abetlen/llama-cpp-python.git && \
        cd llama-cpp-python && \
        if [ ! "${BASE_IMAGE}" = "python:3.8-slim" ]; then \
            CMAKE_ARGS="-DLLAMA_CUBLAS=on" FORCE_CMAKE=1 python3 -m pip install --user . ;\
        else \
            CMAKE_ARGS="-DLLAMA_BLAS=on -DLLAMA_BLAS_VENDOR=OpenBLAS" FORCE_CMAKE=1 python3 -m pip install --user . ; \
        fi && \
        cd .. && \
        rm -rf llama-cpp-python && \
        rm /tmp/requirements.txt'

COPY --chown=user:user . /home/user

WORKDIR /home/user

ARG HTTP_PORT=8000
ENV HTTP_PORT=${HTTP_PORT}

ARG GRPC_PORT=8001
ENV GRPC_PORT=${GRPC_PORT}

ARG METRICS_PORT=8002
ENV METRICS_PORT=${METRICS_PORT}

EXPOSE ${HTTP_PORT}
EXPOSE ${GRPC_PORT}
EXPOSE ${METRICS_PORT}

VOLUME [ "/home/user/data" ]

ENTRYPOINT [ "sh", "/home/user/scripts/entrypoint.sh" ]

CMD [ "python3", "-m", "app.main" ]
