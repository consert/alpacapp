"""Inference."""
import os
import sys
from pathlib import Path
from typing import Callable, Dict, List, Optional, Union

import numpy as np
from langchain import PromptTemplate
from langchain.chains import ConversationChain
from langchain.chains.conversation.prompt import PROMPT
from langchain.llms.base import LLM
from langchain.memory import ConversationBufferWindowMemory
from langchain.schema import BaseMemory
from pytriton.proxy.types import Request
from pytriton.triton import Tensor

try:
    from app.log import DEBUG, get_logger
except ImportError:
    sys.path.append(str(Path(__file__).parent.parent))
    from app.log import DEBUG, get_logger


LOG = get_logger("app::infer", level="INFO" if DEBUG else "DEBUG")
MODEL_CHAT_HISTORY_STR = os.environ.get('MODEL_CHAT_HISTORY', '10')
if not MODEL_CHAT_HISTORY_STR:
    MODEL_CHAT_HISTORY_STR = '10'
try:
    MODEL_CHAT_HISTORY = int(MODEL_CHAT_HISTORY_STR)
except ValueError:
    MODEL_CHAT_HISTORY = 10

MEMORY_KEY = os.environ.get('MEMORY_KEY', 'chat_history')
AI_PREFIX = os.environ.get('AI_PREFIX', 'Plato')
HUMAN_PREFIX = os.environ.get('HUMAN_PREFIX', 'Human')
CLEANUP_WORDS = ['exit', 'clear']


def to_string(thing: Union[str, bytes, np.ndarray, Tensor]) -> str:
    """Convert a thing to a string."""
    if isinstance(thing, Tensor):
        thing = thing.as_numpy()
    if isinstance(thing, str):
        return thing
    if isinstance(thing, bytes):
        return thing.decode("utf-8")
    if isinstance(thing, np.ndarray):
        thing_ = thing[0]
        if isinstance(thing_, bytes):
            return thing_.decode("utf-8")
    return str(thing)


class InferCallable:
    """Callable for inference.

    https://triton-inference-server.github.io/pytriton/0.2.0/inference_callable/
    """

    def __init__(
        self,
        model: LLM,
        memory: Optional[BaseMemory] = None,
        prompt: Optional[PromptTemplate] = None,
        before_call: Optional[Callable[[List[str]], List[str]]] = None,
    ) -> None:
        """Callable class to use for inference.

        Parameters
        ----------
        model : LLM
            The language model to use.
        memory : Optional[BaseMemory], optional
            The memory to use, by default None
        prompt : Optional[PromptTemplate], optional
            The prompt to use, by default None
        before_call : Optional[Callable[[List[str]], List[str]]], optional
            A function to call before the model is called, by default None
        """
        self.model = model
        if memory is None:
            memory = ConversationBufferWindowMemory(
                k=MODEL_CHAT_HISTORY,
                memory_key=MEMORY_KEY,
                ai_prefix=AI_PREFIX,
                human_prefix=HUMAN_PREFIX,
            )
        self.memory = memory
        if prompt is None:
            prompt = PROMPT
        self.conversation = ConversationChain(
            llm=model,
            verbose=DEBUG,
            memory=self.memory,
            prompt=prompt,
        )
        self.before_call = before_call

    @property
    def inputs(self) -> List[Tensor]:
        """Get the inputs."""
        return [Tensor(name="INPUT0", shape=(1,), dtype=bytes)]

    @property
    def outputs(self) -> List[Tensor]:
        """Get the outputs."""
        return [
            Tensor(name="OUTPUT0", shape=(1,), dtype=bytes),
        ]

    @staticmethod
    def cleanup_output(output: str) -> str:
        """Cleanup output.

        If the response is like:

        Plato: <response>

        then we want to remove the "Plato: " part.
        and remove any leading or trailing whitespace.
        """
        cleaned = output.strip()
        if output.startswith(os.path.sep):
            cleaned = output[len(os.path.sep) :]
        if cleaned.startswith(f'{AI_PREFIX}:'):
            # fmt: off
            cleaned = cleaned[len(AI_PREFIX) + 1:]
            # fmt: on
        if cleaned.startswith(os.path.sep):
            cleaned = cleaned[len(os.path.sep) :]
        if cleaned.endswith(os.path.sep):
            cleaned = cleaned[: -len(os.path.sep)]
        return cleaned.strip()

    @staticmethod
    def get_user_inputs_from_request(request: Request) -> List[str]:
        """Get user inputs from request."""
        user_input_data = request.data["INPUT0"]
        user_inputs: List[str] = []
        for user_input in user_input_data:
            if len(user_input) > 1 and len(user_input[0]) == 1:
                user_inputs.append(to_string(user_input[0]))
            else:
                user_inputs.append(to_string(user_input))
        return user_inputs

    def __call__(self, requests: List[Request]) -> List[Dict[str, np.ndarray]]:
        """Call."""
        user_inputs = []  # type: List[str]
        clear_history = False
        for request in requests:
            user_inputs.extend(self.get_user_inputs_from_request(request))
            #  we might want to get extra parameters from the request (like a flag to clear history)
            #  e.g.:
            params = getattr(request, 'parameters', None)
            clear_history_param = params and params.get('clear_history', "")
            clear_history_param_str = str(clear_history_param)
            if clear_history_param_str:
                clear_history |= clear_history_param_str.lower() == "true"
        if user_inputs and user_inputs[0].strip().lower() in CLEANUP_WORDS:
            LOG.info("clearing the chat history ...")
            self.memory.clear()
            return [{"OUTPUT0": np.array([""])}]
        if clear_history:
            self.memory.clear()
        if self.before_call is not None:
            user_inputs = self.before_call(user_inputs)
        model_outputs = []  # type: List[str]
        for user_input in user_inputs:
            model_output = self.conversation.predict(input=user_input)
            if isinstance(model_output, list):
                model_output = model_output[0]
            if not isinstance(model_output, str):
                model_output = str(model_output)
            model_output = self.cleanup_output(model_output)
            model_outputs.append(model_output)
        if not model_outputs:
            LOG.warning("No outputs")
            model_outputs.append("")
        return [{"OUTPUT0": np.array([user_output for user_output in model_outputs])}]
