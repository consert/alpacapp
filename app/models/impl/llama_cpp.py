"""LlamaCpp model and configuration.

https://python.langchain.com/en/latest/modules/models/llms/integrations/llamacpp.html
"""
import os
from pathlib import Path
from typing import Optional, Tuple

from langchain import PromptTemplate
from langchain.llms import LlamaCpp

HERE = Path(__file__).parent.resolve()
ROOT_DIR = HERE.parent.parent.parent

DATA_DIR = Path(os.environ.get("DATA_DIR", str(ROOT_DIR / "data")))
if not DATA_DIR:
    DATA_DIR = ROOT_DIR / "data"

# parameters for the model, overridable with env vars
LLAMA_MODEL_N_GPU_LAYERS = int(os.environ.get("LLAMA_MODEL_N_GPU_LAYERS", "32"))
LLAMA_MODEL_N_BATCH = int(os.environ.get("LLAMA_MODEL_N_BATCH", "8"))
LLAMA_MODEL_TEMPERATURE = float(os.environ.get("LLAMA_MODEL_TEMPERATURE", "0.0"))
LLAMA_MODEL_TOP_P = float(os.environ.get("LLAMA_MODEL_TOP_P", "0.9"))
LLAMA_MODEL_TOP_K = int(os.environ.get("LLAMA_MODEL_TOP_K", "40"))
LLAMA_MODEL_REPEAT_PENALTY = float(os.environ.get("LLAMA_MODEL_REPEAT_PENALTY", "1.1"))
LLAMA_MODEL_MAX_TOKENS = int(os.environ.get("LLAMA_MODEL_MAX_TOKENS", "256"))
LLAMA_MODEL_N_CONTEXT = int(os.environ.get("LLAMA_MODEL_N_CONTEXT", "2048"))

_USE_GPU_ENV = os.environ.get("LLAMA_MODEL_USE_GPU", "true")
if not _USE_GPU_ENV:
    _USE_GPU_ENV = "true"
USE_GPU = _USE_GPU_ENV.lower()[0] in ("t", "y", "1")

_USE_LLAMA_ENV = os.environ.get("USE_LLAMA_CPP", "false")
USE_LLAMA_CPP = _USE_LLAMA_ENV.lower()[0] in ("t", "y", "1")

_MODEL_FILENAME_ENV = os.environ.get("LLAMA_MODEL_FILENAME", "")
if not _MODEL_FILENAME_ENV:
    _MODEL_FILENAME_ENV = "model.bin" if USE_GPU else "model-q4_0.bin"
MODEL_FILENAME = _MODEL_FILENAME_ENV
MODEL_PATH = os.environ.get("LLAMA_MODEL_PATH", str(DATA_DIR / MODEL_FILENAME))
if not MODEL_PATH:
    MODEL_PATH = str(DATA_DIR / MODEL_FILENAME)

if not os.path.exists(MODEL_PATH):
    raise RuntimeError(f"Model not found at {MODEL_PATH}")

_KWARGS = {
    "model_path": MODEL_PATH,
    "n_batch": LLAMA_MODEL_N_BATCH,
    "temperature": LLAMA_MODEL_TEMPERATURE,
    "top_p": LLAMA_MODEL_TOP_P,
    "top_k": LLAMA_MODEL_TOP_K,
    "repeat_penalty": LLAMA_MODEL_REPEAT_PENALTY,
    "max_tokens": LLAMA_MODEL_MAX_TOKENS,
    'n_ctx': LLAMA_MODEL_N_CONTEXT,
}
if USE_GPU:
    _KWARGS["n_gpu_layers"] = LLAMA_MODEL_N_GPU_LAYERS
VERSION_STR = os.environ.get("LLAMA_MODEL_VERSION", "1")
if not VERSION_STR:
    VERSION_STR = "1"
try:
    MODEL_VERSION = int(VERSION_STR)
except ValueError:
    MODEL_VERSION = 1
MODEL_NAME = os.environ.get("LLAMA_MODEL_NAME", "alpaca")
if not MODEL_NAME:
    MODEL_NAME = "alpaca"

LLAMA_MODEL: Optional[LlamaCpp] = None
if USE_LLAMA_CPP:
    LLAMA_MODEL = LlamaCpp(
        **_KWARGS,
    )


def get_model() -> Optional[Tuple[str, int, LlamaCpp, Optional[PromptTemplate]]]:
    """Get a llama_cpp model."""
    if USE_LLAMA_CPP is False or LLAMA_MODEL is None:
        return None
    return (
        MODEL_NAME,
        MODEL_VERSION,
        LLAMA_MODEL,
        None,
    )


if __name__ == "__main__":
    get_model()
