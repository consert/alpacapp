"""Alpaca model.

https://huggingface.co/chainyo/alpaca-lora-7b
"""

import os
from typing import Optional, Tuple

import torch
from langchain import PromptTemplate
from langchain.llms import HuggingFacePipeline
from transformers import LlamaForCausalLM, LlamaTokenizer, pipeline

DEFAULT_ALPACA_REPO_ID = "chainyo/alpaca-lora-7b"
USE_ALPACA = os.environ.get("USE_ALPACA", "false").lower() == "true"
ALPACA_REPO_ID = "" if not USE_ALPACA else DEFAULT_ALPACA_REPO_ID
if USE_ALPACA:
    ALPACA_REPO_ID = os.environ.get("ALPACA_REPO_ID", DEFAULT_ALPACA_REPO_ID)
    if not ALPACA_REPO_ID:
        ALPACA_REPO_ID = DEFAULT_ALPACA_REPO_ID

ALPACA_MODEL_VERSION_STR = os.environ.get("ALPACA_MODEL_VERSION", "1")
if not ALPACA_MODEL_VERSION_STR:
    ALPACA_MODEL_VERSION_STR = "1"
try:
    ALPACA_MODEL_VERSION = int(ALPACA_MODEL_VERSION_STR)
except ValueError:
    ALPACA_MODEL_VERSION = 1
ALPACA_MODEL_NAME = os.environ.get("ALPACA_MODEL_NAME", "alpaca")
if not ALPACA_MODEL_NAME:
    ALPACA_MODEL_NAME = "alpaca"
ALPACA_MODEL_TOP_K = int(os.environ.get("ALPACA_MODEL_TOP_K", "10"))
ALPACA_MODEL_TEMPERATURE = float(os.environ.get("ALPACA_MODEL_TEMPERATURE", "0.1"))
ALPACA_MODEL_MAX_LENGTH = int(os.environ.get("ALPACA_MODEL_MAX_LENGTH", "256"))
ALPACA_LOAD_IN_8_BIT = os.environ.get('ALPACA_LOAD_IN_8_BIT', '').lower() == 'true'
ALPACA_MODEL_TOP_P = float(os.environ.get("ALPACA_MODEL_TOP_P", "0.95"))
ALPACA_MODEL_TOP_K = int(os.environ.get("ALPACA_MODEL_TOP_K", "10"))
ALPACA_MODEL_NUM_BEAMS = int(os.environ.get("ALPACA_MODEL_NUM_BEAMS", "1"))
ALPACA_MODEL_MAX_NEW_TOKENS = int(os.environ.get("ALPACA_MODEL_MAX_NEW_TOKENS", "1024"))
ALPACA_MODEL_REPETITION_PENALTY = float(os.environ.get("ALPACA_MODEL_REPETITION_PENALTY", "1.2"))

tokenizer = LlamaTokenizer.from_pretrained(ALPACA_REPO_ID)
base_model = LlamaForCausalLM.from_pretrained(
    ALPACA_REPO_ID,
    load_in_8bit=ALPACA_LOAD_IN_8_BIT,
    torch_dtype=torch.float16,
    device_map="auto",
)

pipe = pipeline(
    "text-generation",
    model=base_model,
    tokenizer=tokenizer,
    max_length=ALPACA_MODEL_MAX_LENGTH,
    max_new_tokens=ALPACA_MODEL_MAX_NEW_TOKENS,
    temperature=ALPACA_MODEL_TEMPERATURE,
    top_p=ALPACA_MODEL_TOP_P,
    top_k=ALPACA_MODEL_TOP_K,
    num_beams=ALPACA_MODEL_NUM_BEAMS,
    repetition_penalty=ALPACA_MODEL_REPETITION_PENALTY,
)


def get_model() -> Optional[Tuple[str, int, HuggingFacePipeline, Optional[PromptTemplate]]]:
    """Get the model to use for inference."""
    if not USE_ALPACA:
        return None
    llm = HuggingFacePipeline(
        pipeline=pipe,
    )
    return ALPACA_MODEL_NAME, ALPACA_MODEL_VERSION, llm, None


if __name__ == "__main__":
    _ = HuggingFacePipeline(
        pipeline=pipe,
    )
