"""Falcon model using huggingface hub.

https://python.langchain.com/en/latest/modules/models/llms/integrations/huggingface_hub.html
https://huggingface.co/tiiuae/falcon-7b-instruct
"""
import os
from typing import Optional, Tuple

from langchain import HuggingFaceHub, PromptTemplate

HF_MODEL_TOP_K = int(os.environ.get("HF_MODEL_TOP_K", "10"))
HF_MODEL_TEMPERATURE = float(os.environ.get("HF_MODEL_TEMPERATURE", "0.1"))
HF_MODEL_MAX_LENGTH = int(os.environ.get("HF_MODEL_MAX_LENGTH", "2048"))

if HF_MODEL_TEMPERATURE <= 0.0:
    # Avoid: validation error: `temperature` must be strictly positive
    HF_MODEL_TEMPERATURE = 0.1


MEMORY_KEY = os.environ.get('MEMORY_KEY', 'memory')
AI_PREFIX = os.environ.get('AI_PREFIX', 'Plato')
HUMAN_PREFIX = os.environ.get('HUMAN_PREFIX', 'Human')


def get_prompt() -> Optional[PromptTemplate]:
    """Get the prompt template to use for inference.

    Use the default in ../../prompt.py if not set.
    """
    return None


def get_model() -> Optional[Tuple[str, int, HuggingFaceHub, Optional[PromptTemplate]]]:
    """Get the model to use for inference."""
    use_hf_hub = os.environ.get("USE_HF_HUB", "false").lower() == "true"
    if not use_hf_hub:
        return None
    hf_token = os.environ.get('HUGGINGFACEHUB_API_TOKEN', None)
    if not hf_token:
        return None
    repo_id = os.environ.get("HF_REPO_ID", "tiiuae/falcon-7b-instruct")
    # 40b seems to take forever to return a response
    # repo_id = os.environ.get("HF_REPO_ID", "tiiuae/falcon-40b-instruct")
    model_kwargs = {
        "top_k": HF_MODEL_TOP_K,
        "temperature": HF_MODEL_TEMPERATURE,
        "max_length": HF_MODEL_MAX_LENGTH,
        "max_new_tokens": HF_MODEL_MAX_LENGTH,
        "do_sample": True,
        "num_return_sequences": 1,
    }
    model = HuggingFaceHub(repo_id=repo_id, task='text-generation', model_kwargs=model_kwargs)
    # make sure "falcon" as name is not loaded with ./falcon.py too. (i.e. only load one of them)
    model_name = os.environ.get("HF_MODEL_NAME", "falcon")
    if not model_name:
        model_name = "falcon"
    model_version_str = os.environ.get("HF_MODEL_VERSION", "1")
    if not model_version_str:
        model_version_str = "1"
    try:
        model_version = int(model_version_str)
    except ValueError:
        model_version = 1
    return model_name, model_version, model, get_prompt()


if __name__ == "__main__":
    get_model()
