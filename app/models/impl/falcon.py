"""Falcon model.

https://huggingface.co/tiiuae/falcon-7b
"""

import os
from typing import Any, List, Mapping, Optional, Tuple

import torch
import transformers
from langchain import PromptTemplate
from langchain.callbacks.manager import CallbackManagerForLLMRun
from langchain.llms.base import LLM
from transformers import AutoTokenizer

DEFAULT_FALCON_REPO_ID = "tiiuae/falcon-7b-instruct"
USE_FALCON = os.environ.get("USE_FALCON", "false").lower() == "true"
FALCON_REPO_ID = "" if not USE_FALCON else DEFAULT_FALCON_REPO_ID
if USE_FALCON:
    FALCON_REPO_ID = os.environ.get("FALCON_REPO_ID", DEFAULT_FALCON_REPO_ID)
    if not FALCON_REPO_ID:
        FALCON_REPO_ID = DEFAULT_FALCON_REPO_ID

FALCON_MODEL_VERSION_STR = os.environ.get("FALCON_MODEL_VERSION", "1")
if not FALCON_MODEL_VERSION_STR:
    FALCON_MODEL_VERSION_STR = "1"
try:
    FALCON_MODEL_VERSION = int(FALCON_MODEL_VERSION_STR)
except ValueError:
    FALCON_MODEL_VERSION = 1

FALCON_MODEL_TOP_K = int(os.environ.get("FALCON_MODEL_TOP_K", "10"))
FALCON_MODEL_TEMPERATURE = float(os.environ.get("FALCON_MODEL_TEMPERATURE", "0.1"))
FALCON_MODEL_MAX_LENGTH = int(os.environ.get("FALCON_MODEL_MAX_LENGTH", "1024"))

model = FALCON_REPO_ID

tokenizer = AutoTokenizer.from_pretrained(model)
pipeline = transformers.pipeline(
    "text-generation",
    # "conversational",
    model=model,
    tokenizer=tokenizer,
    torch_dtype=torch.bfloat16,
    trust_remote_code=True,
    device_map="auto",
)


class FalconLLM(LLM):
    """Falcon model.

    https://huggingface.co/tiiuae/falcon-7b-instruct
    """

    @property
    def _identifying_params(self) -> Mapping[str, Any]:
        return {
            "model": model,
        }

    @property
    def _llm_type(self) -> str:
        return "custom"

    def _call(
        self,
        prompt: str,
        stop: Optional[List[str]] = None,
        run_manager: Optional[CallbackManagerForLLMRun] = None,
    ) -> str:
        sequences = pipeline(
            prompt,
            max_length=FALCON_MODEL_MAX_LENGTH,
            do_sample=True,
            top_k=FALCON_MODEL_TOP_K,
            num_return_sequences=1,
            eos_token_id=tokenizer.eos_token_id,
        )
        if not sequences:
            return ""
        response = sequences[0]["generated_text"].replace(prompt, "").strip()
        if response and response[-1] == tokenizer.eos_token:
            response = response[:-1]
        if response and response.startswith(os.path.sep):
            # fmt: off
            response = response[len(os.path.sep):]
            # fmt: on
        return response


FALCON_LLM: Optional[FalconLLM] = None

if USE_FALCON:
    FALCON_LLM = FalconLLM()


def get_model() -> Optional[Tuple[str, int, FalconLLM, Optional[PromptTemplate]]]:
    """Get the model to use for inference."""
    if not USE_FALCON or FALCON_LLM is None:
        return None
    return "falcon", FALCON_MODEL_VERSION, FALCON_LLM, None


if __name__ == "__main__":
    # init (download model)
    _ = FalconLLM()
