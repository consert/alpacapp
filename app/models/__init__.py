"""Get the model(s) to use for inference."""

import os
import sys
from importlib import import_module
from pathlib import Path
from typing import List, Optional, Tuple

from dotenv import load_dotenv
from langchain import PromptTemplate
from langchain.llms.base import LLM

load_dotenv()


# pylint: disable=too-many-try-statements
try:
    from app.log import get_logger
except ImportError:
    sys.path.append(str(Path(__file__).parent.parent))
    from app.log import get_logger


LOG = get_logger("app::models")


def get_model_files() -> List[str]:
    """Get the model files."""
    dir_files = os.listdir((Path(__file__).parent / "impl").resolve())
    possible_model_files = [
        file for file in dir_files if file.endswith(".py") and file != "__init__.py"
    ]
    # since we don't want to load all the models in the models/impl directory,
    #  we only want to load the ones that are marked to be loaded from the env
    model_files = []
    for model_file in possible_model_files:
        model_name = model_file[:-3]
        if os.environ.get(f"USE_{model_name.upper()}", "false").lower() == "true":
            model_files.append(model_file)
    return [model_file[:-3] for model_file in model_files]


def get_models() -> List[Tuple[str, int, LLM, Optional[PromptTemplate]]]:
    """Get the model to use for inference.

    Returns
    -------
    List[Tuple[str, int, LLM]]
        A list of tuples containing the model name, version, and LLM.

    """
    models = []  # type: List[Tuple[str, int, LLM, Optional[PromptTemplate]]]
    model_files = get_model_files()
    LOG.debug("Found model files: %s", model_files)
    for model_file in model_files:
        try:
            module = import_module(f"app.models.impl.{model_file}")
            # pylint: disable=broad-except
        except Exception as error:
            LOG.error("Failed to import model %s, %s", model_file, error)
            continue
        if not hasattr(module, "get_model"):
            LOG.warning("Model %s has no get_model function", model_file)
            continue
        try:
            model_tuple = module.get_model()
            # pylint: disable=broad-except
        except Exception as error:
            LOG.error("Failed to get model %s, %s", model_file, error)
            continue
        if not model_tuple:
            LOG.warning("Model %s returned None", model_file)
            continue
        models.append(model_tuple)
    if not models:
        LOG.error("No models found.")
        # raise RuntimeError("No models found.")
    return models


__all__ = ["get_models"]
