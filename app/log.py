"""Logging configuration.

borrowed from click and uvicorn
"""
# pylint: disable=too-many-arguments,too-many-locals,too-many-branches,too-many-statements, no-self-use,invalid-name
import logging
import os
import sys
from copy import copy
from logging.config import dictConfig
from typing import Any, Dict, Optional, Tuple, Union

DEBUG = os.environ.get("DEBUG", "false").lower()[0] in ("t", "y", "1")
DEFAULT_LOG_FORMAT = "%(levelprefix)s %(name)s %(asctime)s     %(message)s"
LOG_LEVEL = logging.DEBUG if DEBUG else logging.INFO


_ansi_colors = {
    "black": 30,
    "red": 31,
    "green": 32,
    "yellow": 33,
    "blue": 34,
    "magenta": 35,
    "cyan": 36,
    "white": 37,
    "reset": 39,
    "bright_black": 90,
    "bright_red": 91,
    "bright_green": 92,
    "bright_yellow": 93,
    "bright_blue": 94,
    "bright_magenta": 95,
    "bright_cyan": 96,
    "bright_white": 97,
}
_ansi_reset_all = "\033[0m"


def _interpret_color(color: Union[int, Tuple[int, int, int], str], offset: int = 0) -> str:
    if isinstance(color, int):
        return f"{38 + offset};5;{color:d}"

    if isinstance(color, (tuple, list)):
        r, g, b = color
        return f"{38 + offset};2;{r:d};{g:d};{b:d}"

    return str(_ansi_colors[color] + offset)


# pylint: disable=too-complex
def click_style(  # noqa: C901
    text: Any,
    fg: Optional[Union[int, Tuple[int, int, int], str]] = None,
    bg: Optional[Union[int, Tuple[int, int, int], str]] = None,
    bold: Optional[bool] = None,
    dim: Optional[bool] = None,
    underline: Optional[bool] = None,
    overline: Optional[bool] = None,
    italic: Optional[bool] = None,
    blink: Optional[bool] = None,
    reverse: Optional[bool] = None,
    strikethrough: Optional[bool] = None,
    reset: bool = True,
) -> str:
    """Styles a text with ANSI styles and returns the new string.

    source/credits:
    https://github.com/pallets/click/blob/main/src/click/termui.py
    """
    if not isinstance(text, str):
        text = str(text)

    bits = []

    if fg:
        try:
            bits.append(f"\033[{_interpret_color(fg)}m")
        except KeyError:
            raise TypeError(f"Unknown color {fg!r}") from None

    if bg:
        try:
            bits.append(f"\033[{_interpret_color(bg, 10)}m")
        except KeyError:
            raise TypeError(f"Unknown color {bg!r}") from None

    if bold is not None:
        bits.append(f"\033[{1 if bold else 22}m")
    if dim is not None:
        bits.append(f"\033[{2 if dim else 22}m")
    if underline is not None:
        bits.append(f"\033[{4 if underline else 24}m")
    if overline is not None:
        bits.append(f"\033[{53 if overline else 55}m")
    if italic is not None:
        bits.append(f"\033[{3 if italic else 23}m")
    if blink is not None:
        bits.append(f"\033[{5 if blink else 25}m")
    if reverse is not None:
        bits.append(f"\033[{7 if reverse else 27}m")
    if strikethrough is not None:
        bits.append(f"\033[{9 if strikethrough else 29}m")
    bits.append(text)
    if reset:
        bits.append(_ansi_reset_all)
    return "".join(bits)


class ColorizedFormatter(logging.Formatter):  # type: ignore
    """
    Custom log formatter.

    A custom log formatter class that:
    * Outputs the LOG_LEVEL with an appropriate color.
    * If a log call includes an `extras={"color_message": ...}` it will be used
      for formatting the output, instead of the plain text message.
    """

    TRACE_LOG_LEVEL = 5

    level_name_colors = {
        TRACE_LOG_LEVEL: lambda level_name: click_style(str(level_name), fg="blue"),
        logging.DEBUG: lambda level_name: click_style(str(level_name), fg="cyan"),
        logging.INFO: lambda level_name: click_style(str(level_name), fg="green"),
        logging.WARNING: lambda level_name: click_style(str(level_name), fg="yellow"),
        logging.ERROR: lambda level_name: click_style(str(level_name), fg="red"),
        logging.CRITICAL: lambda level_name: click_style(str(level_name), fg="bright_red"),
    }

    def __init__(
        self,
        fmt=None,  # type: Optional[str]
        datefmt=None,  # type: Optional[str]
        style="%",  # type: str
        use_colors=None,  # type: Optional[bool]
    ):
        """Initialize."""
        if use_colors in (True, False):
            self.use_colors = use_colors
        elif hasattr(sys.stdout, 'isatty'):
            self.use_colors = sys.stdout.isatty()
        else:
            self.use_colors = True
        super().__init__(fmt=fmt, datefmt=datefmt, style=style)  # type: ignore

    def color_level_name(self, level_name: str, level_no: int) -> str:
        """Get color level name."""

        def default(_level_name: str) -> str:
            """Return the default color level name."""
            return str(_level_name)  # pragma: no cover

        func = self.level_name_colors.get(level_no, default)
        return func(level_name)

    def should_use_colors(self) -> bool:
        """Check if colors should be used."""
        return True  # pragma: no cover

    def formatMessage(self, record: logging.LogRecord) -> str:
        """Format the message."""
        record_copy = copy(record)
        levelname = record_copy.levelname
        separator = " " * (8 - len(record_copy.levelname))
        if self.use_colors:
            levelname = self.color_level_name(levelname, record_copy.levelno)
            if "color_message" in record_copy.__dict__:
                record_copy.msg = record_copy.__dict__["color_message"]
                record_copy.__dict__["message"] = record_copy.getMessage()
        if levelname != "INFO":
            record_copy.__dict__["message"] = record_copy.getMessage()
        record_copy.__dict__["levelprefix"] = levelname + ":" + separator
        return super().formatMessage(record_copy)


LOGGING_CONFIG: Dict[str, Any] = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {
            "()": ColorizedFormatter,
            "fmt": DEFAULT_LOG_FORMAT,
            "datefmt": "%Y-%m-%d %H:%M:%S",
        }
    },
    "handlers": {
        "default": {
            "class": "logging.StreamHandler",
            "formatter": "default",
            "stream": "ext://sys.stdout",
        }
    },
    "loggers": {"": {"handlers": ["default"], "level": "INFO", "propagate": False}},
}

dictConfig(LOGGING_CONFIG)


def get_logger(name: str, level: Optional[str] = None) -> logging.Logger:
    """Get a logger.

    Parameters
    ----------
    name : str
        The name of the logger.
    level : str, optional
        The level of the logger, by default "INFO"
        Available levels are: DEBUG, INFO, WARNING, ERROR, CRITICAL
    """
    log_level_str = "DEBUG" if LOG_LEVEL == logging.DEBUG else "INFO"
    if level is None:
        level = log_level_str
    from_env = os.environ.get("LOG_LEVEL", "none").upper()
    if level is not None and level not in (
        "DEBUG",
        "INFO",
        "WARNING",
        "ERROR",
        "CRITICAL",
    ):
        level = "INFO"
    if level is None:
        level = (
            from_env
            if from_env in ("DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL")
            else log_level_str
        )
    if LOGGING_CONFIG["loggers"][""]["level"] != level:
        updated_config = copy(LOGGING_CONFIG)
        updated_config["loggers"][""]["level"] = level
        dictConfig(updated_config)
    logger = logging.getLogger(name)
    logger.setLevel(level)
    return logger


__all__ = ["get_logger"]
