"""Prompt for the instruction task."""
# pylint: disable=line-too-long
import json
import os
from typing import List, Tuple

from langchain.prompts.prompt import PromptTemplate

MEMORY_KEY = os.environ.get('MEMORY_KEY', 'chat_history')
AI_PREFIX = os.environ.get('AI_PREFIX', 'Plato')
HUMAN_PREFIX = os.environ.get('HUMAN_PREFIX', 'Human')

# how the model was trained:
ORIGINAL_PROMPT = """Below is an instruction that describes a task, paired with an input that provides further context. Write a response that appropriately completes the request.

### Instruction:
{instruction}

### Input:
{input_ctx}

### Response:
"""


def load_prompt_details() -> Tuple[str, str, List[str]]:
    """Load instruction and initial messages."""
    here = os.path.realpath(os.path.join(__file__, ".."))
    prompt_json = os.path.join(here, "prompt.json")
    prompt_override = os.path.join(here, "prompt_override.json")
    instruction = ""
    initial_messages_header = "Conversation History:"
    initial_messages = []  # type: List[str]
    file_to_load = prompt_override if os.path.exists(prompt_override) else prompt_json
    if os.path.exists(file_to_load):
        with open(file_to_load, "r", encoding="utf-8") as _in:
            prompt = json.load(_in)
        instruction = prompt.get("instruction", instruction)
        initial_messages = prompt.get("initial_messages", initial_messages)
        initial_messages_header = prompt.get("initial_messages_header", initial_messages_header)
        return instruction, initial_messages_header, initial_messages
    raise FileNotFoundError("Could not get the prompt form json")


INSTRUCTION, INITIAL_MESSAGES_HEADER, INITIAL_MESSAGES = load_prompt_details()
INITIAL_MESSAGES_STR = "\n".join(INITIAL_MESSAGES)
INPUT_CTX = f"""{INITIAL_MESSAGES_HEADER}
{INITIAL_MESSAGES_STR}
{'{' + MEMORY_KEY + '}'}

{HUMAN_PREFIX + ': {input}'}
{AI_PREFIX}:
"""
TEMPLATE = ORIGINAL_PROMPT.format(instruction=INSTRUCTION, input_ctx=INPUT_CTX)


def get_default_prompt() -> PromptTemplate:
    """Get the instruction template."""
    return PromptTemplate(input_variables=[MEMORY_KEY, "input"], template=TEMPLATE)
