"""Main module for the app."""
import os
import sys
from pathlib import Path

from pytriton.model_config import ModelConfig  # type: ignore
from pytriton.triton import Triton, TritonConfig  # type: ignore

try:
    from app.log import DEBUG, get_logger
except ImportError:
    sys.path.append(str(Path(__file__).parent.parent))
    from app.log import DEBUG, get_logger

from app.infer import InferCallable
from app.models import get_models
from app.prompt import get_default_prompt

LOG = get_logger("app::main", level="INFO" if DEBUG else "DEBUG")

BIND_ADDRESS = os.environ.get("BIND_ADDRESS", "0.0.0.0")
HTTP_PORT = int(os.environ.get("HTTP_PORT", "8000"))
GRPC_PORT = int(os.environ.get("GRPC_PORT", "8001"))
METRICS_PORT = int(os.environ.get("METRICS_PORT", "8002"))
MAX_BATCH_SIZE = int(os.environ.get("MAX_BATCH_SIZE", "128"))
ALLOW_METRICS = os.environ.get("ALLOW_METRICS", "true")
ALLOW_CPU_METRICS = os.environ.get("ALLOW_CPU_METRICS", "true")
ALLOW_GPU_METRICS = os.environ.get("ALLOW_GPU_METRICS", "true")


def main() -> None:
    """Start the server."""
    triton_config = TritonConfig(
        http_address=BIND_ADDRESS,
        http_port=HTTP_PORT,
        log_verbose=9 if DEBUG else 0,
        allow_metrics=str(ALLOW_METRICS).lower() == "true",
        allow_cpu_metrics=str(ALLOW_CPU_METRICS).lower() == "true",
        allow_gpu_metrics=str(ALLOW_GPU_METRICS).lower() == "true",
        grpc_address=BIND_ADDRESS,
        grpc_port=GRPC_PORT,
        metrics_port=METRICS_PORT,
        allow_grpc=True,
        allow_http=True,
    )
    models = get_models()
    with Triton(config=triton_config) as triton:
        for model in models:
            model_name, model_version, llm, prompt = model
            if prompt is None:
                prompt = get_default_prompt()
            infer_callable = InferCallable(llm, prompt=prompt)
            triton.bind(
                model_name=model_name,
                model_version=model_version,
                infer_func=infer_callable,
                inputs=infer_callable.inputs,
                outputs=infer_callable.outputs,
                config=ModelConfig(max_batch_size=MAX_BATCH_SIZE),
            )
        triton.serve()


if __name__ == "__main__":
    LOG.info("Starting Triton server...")
    main()
