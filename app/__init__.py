"""App package."""
import os

from dotenv import load_dotenv

load_dotenv()

DATA_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), "..", "data"))

HF_HOME = os.environ.get("HF_HOME", DATA_DIR)
os.environ["HF_HOME"] = HF_HOME

__version__ = "0.0.3"
