# Alpacapp

LLM deployment using [nvidia-pytriton](https://triton-inference-server.github.io/pytriton/0.2.0/) and [langchain](https://python.langchain.com/en/latest/)

## Quick start

### Docker

AIO as a [systemd service](./scripts/service.sh)

Assuming that docker (or `alias docker=podman`), that the nvidia-runtime is installed and is the default runtime, and that the current user is a member of the docker group:

```shell
curl https://gitlab.com/consert/alpacapp/-/raw/main/scripts/get.sh | bash
```

### Kubernetes deployment

Make sure the built docker image can be pulled (either using a local repository or upload it to a registry).

Example call using helm (modify the model to use, the repo, or any of the [./kubernetes/helm/values.yaml](./kubernetes/helm/values.yaml)):

```shell
# cd /path/to/repo/kubernetes/helm
helm install alpacapp . \
  --set deployment.image.repository=localhost:5000/alpacapp \
  --set deployment.image.tag=0.0.1_cuda \
  --set env.MODEL_FILENAME=model-q8_0.bin \
  --set deployment.pullPolicy=Never \
  --create-namespace
```

## Requirements

[./requirements/requirements.txt](./requirements/requirements.txt)

```text
accelerate==0.20.3
bitsandbytes==0.39.0
einops==0.6.1
huggingface-hub==0.15.1
langchain==0.0.188
llama-cpp-python==0.1.57
nvidia-pytriton==0.2.0
peft==0.3.0
python-dotenv==1.0.0
scipy==1.10.1
sentencepiece==0.1.99
torch==2.0.1
transformers==4.30.1
xformers==0.0.20
```

## Additional tools

Models [./app/models/impl/](./app/models/impl/)

- Alpaca <https://huggingface.co/chainyo/alpaca-lora-7b>

  - Using llama.cpp langchain integration <https://python.langchain.com/en/latest/modules/models/llms/integrations/llamacpp.html>
    Converted and quantized with:
    <https://github.com/ggerganov/llama.cpp.git>

    Resulting in `./data/model.bin`, `./data/model-q8_0.bin` and `./data/model-q4_0.bin`  ([./scripts/llama_cpp.sh](./scripts/llama_cpp.sh))

  - Using huggingface pipeline langchain integration <https://python.langchain.com/en/latest/modules/models/llms/integrations/huggingface_pipelines.html>

- Falcon-7b-instruct <https://huggingface.co/tiiuae/falcon-7b-instruct>
  - Using an LLM wrapper <https://python.langchain.com/en/latest/modules/models/llms/examples/custom_llm.html>
  - Using huggingface hub langchain integration <https://python.langchain.com/en/latest/modules/models/llms/integrations/huggingface_hub.html>

## Start Server

```shell
python -m app.main
```

## Example calls

Example python client in [./clients/llm.py](./clients/llm.py)

```shell
python ./clients/llm.py --host localhost --port 30000 --model falcon --message "Who are you?" --clear
```

Clear current conversation history with the `{"clear_history": "true"}` parameter:

```shell
 curl http://127.0.0.1:8000/v2/models/alpaca/infer -H 'Content-Type: application/json' \
-d '{"inputs": [{"name": "INPUT0", "datatype": "BYTES", "shape": [1, 1], "data": ["Who was hist teacher?"]}], "parameters": {"clear_history": "true"}}'
```

```shell
curl http://127.0.0.1:8000/v2/models/alpaca/infer -H 'Content-Type: application/json' \
-d '{"inputs": [{"name": "INPUT0", "datatype": "BYTES", "shape": [1, 1], "data": ["Who was Plato?"]}]}' | jq
```

Response:

```json
{
  "model_name": "alpaca",
  "model_version": "1",
  "outputs": [
    {
      "name": "OUTPUT0",
      "datatype": "BYTES",
      "shape": [
        1
      ],
      "data": [
      "Plato (427-347 BCE) was an ancient Greek philosopher and mathematician who founded the Academy in Athens, the first institution of higher learning in the Western world. He is widely considered to be one of the founders of Western philosophy, particularly the school of thought known as Platonism."
      ]
    }
  ]
}
```

```shell
curl http://127.0.0.1:8000/v2/models/alpaca/infer -H 'Content-Type: application/json' \
-d '{"inputs": [{"name": "INPUT0", "datatype": "BYTES", "shape": [1, 1], "data": ["Who was his teacher?"]}]}' | jq
```

Response:

```json
{
  "model_name": "alpaca",
  "model_version": "1",
  "outputs": [
    {
      "name": "OUTPUT0",
      "datatype": "BYTES",
      "shape": [
        1
      ],
      "data": [
        "Plato's teacher was Socrates (469-399 BCE)."
      ]
    }
  ]
}
```

Use q4_0 model, debug logs:

```shell
DEBUG=true USE_GPU=true MODEL_FILENAME=model-q4_0.bin python -m app.main
```

Logs:

```text
# logs
Prompt after formatting:
Below is an instruction that describes a task, paired with an input that provides further context. Write a response that appropriately completes the request.

### Instruction:
Generate a response given the Conversation History.

### Input:
Conversation History:
Human: Who was Batman?
AI: Batman is a fictional superhero created by Bob Kane and Bill Finger. He first appeared in Detective Comics #27 (May 1939).
Human: Who was his best friend?
AI: Batman's best friend is Robin, a teenage sidekick who has been portrayed by various actors and actresses since the character's debut in Detective Comics #100 (July 1942).
Human: Who was his biggest enemy?
AI: Batman's biggest enemy is The Joker, a clown-themed supervillain who has been portrayed by various actors and actresses since the character's debut in Batman #1 (Spring 1940).
Human: What about Catwoman?
AI: Catwoman is one of Batman's most famous enemies. She first appeared in Detective Comics #133 (July 1950) and has been portrayed by various actors and actresses since then.
Human: OK thanks
AI:

### Response:

```

Use a character (modify the prompt):

![plato](doc/plato.png)

## License

Inheriting the base model's license: (LLaMA [LICENSE](./LICENSE) AGREEMENT)
