"""Text to speech client."""
import argparse

import pyttsx3

engine = pyttsx3.init()


def cli() -> argparse.Namespace:
    """Command line interface."""
    parser = argparse.ArgumentParser(description='Text to speech client.')
    parser.add_argument('text', type=str, help='Text to speak.')
    return parser.parse_args()


def main() -> None:
    """Parse the command line arguments and speak it."""
    args = cli()
    engine.say(args.text)
    engine.runAndWait()


if __name__ == '__main__':
    main()
