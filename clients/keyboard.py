"""Mock server entry point."""
import argparse
import os
import sys
import tempfile
import time
from functools import partial
from multiprocessing import Process
from pathlib import Path

from pynput import keyboard

HERE = Path(__file__).parent


def kill_ps():
    # type: () -> None
    """Kill the process."""
    ps_es = (
        os.popen(f"ps aux | grep {sys.argv[0]} | grep -v grep | awk '{{ print $2 }}'")  # nosec
        .read()
        .split('\n')
    )
    ps_es = [ps for ps in ps_es if ps]
    cmd = f'kill -9 {" ".join(ps_es)}'
    os.system(cmd)  # nosec


def cli():
    # type: () -> argparse.ArgumentParser
    """Get the command line interface."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--daemon', '-d', action='store_true', help='Run as a daemon.')
    parser.add_argument('--stop', '-s', action='store_true', help='Stop the server.')
    parser.add_argument('--status', '-t', action='store_true', help='Get the server status.')
    parser.add_argument('--start-key', '-stk', default='s', help='The key to start sending.')
    parser.add_argument('--stop-key', '-spk', default='t', help='The key to stop sending.')
    parser.add_argument('--abort-key', '-abk', default='esc', help='The key to abort the server.')
    return parser


def do_send():
    # type: () -> None
    """Send the message."""
    Path(HERE / 'keyboard.lock').touch()


def dont_send():
    # type: () -> None
    """Don't send the message."""
    if os.path.exists(HERE / 'keyboard.lock'):
        os.remove(HERE / 'keyboard.lock')


def on_press(key, lock_file, start_key, stop_key, abort_key):
    # type: (keyboard.Key, str, str, str, str) -> None
    """Handle a key press."""
    try:
        k = key.char  # single-char keys
    except AttributeError:
        k = key.name  # other keys
    if k == abort_key:
        print('\nend loop ...\n')
        stop_listening(lock_file)
    if k == start_key:
        print('\ndo send ...\n')
        do_send()
    if k == stop_key:
        dont_send()
        print('\ndont send ...\n')


def start_listening(lock_file, start_key, stop_key, abort_key):
    # type: (str, str, str, str) -> None
    """Start listening for keyboard input."""
    print("Listening for keyboard input...")
    on_press_ = partial(
        on_press, lock_file=lock_file, start_key=start_key, stop_key=stop_key, abort_key=abort_key
    )
    with keyboard.Listener(on_press=on_press_) as listener:
        listener.join()


def stop_listening(lock_file):
    # type: (str) -> None
    """Stop listening for keyboard input."""
    print("Stopping listening for keyboard input.")
    if os.path.exists(lock_file):
        os.remove(lock_file)
    dont_send()
    kill_ps()
    sys.exit(0)


def start(lock_file, start_key, stop_key, abort_key):
    # type: (str, str, str, str) -> None
    """Start the server."""
    start_listening(
        lock_file=lock_file, start_key=start_key, stop_key=stop_key, abort_key=abort_key
    )
    if not os.path.exists(lock_file):
        Path(lock_file).touch()
    while os.path.exists(lock_file):
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            print("Stopping listener...")
            if os.path.exists(lock_file):
                os.remove(lock_file)
            stop_listening(lock_file)
            break
    if os.path.exists(lock_file):
        os.remove(lock_file)
    stop_listening(lock_file)


def start_process(lock_file, start_key, stop_key, abort_key):
    # type: (str, str, str, str) -> None
    """Run the server as a daemon."""
    proc = Process(target=start, args=(lock_file, start_key, stop_key, abort_key))
    proc.daemon = True
    proc.start()
    proc.join()


def status(lock_file):
    # type: (str) -> None
    """Get the listener status."""
    if os.path.exists(lock_file):
        print("listener is running.")
        sys.exit(0)
    else:
        print("listener is not running.")
        sys.exit(1)


def start_daemon(lock_file, start_key, stop_key, abort_key):
    # type: (str, str, str, str) -> None
    """Start the listener as a daemon."""
    pid = os.fork()
    if pid == 0:
        if not os.path.exists(lock_file):
            # child: start listener, let the parent exit
            start_process(lock_file, start_key, stop_key, abort_key)
    else:
        # parent, exit
        sys.exit(0)


def main():
    # type: () -> None
    """Run the listener."""
    args = cli().parse_args()
    _lock_file = os.path.join(tempfile.gettempdir(), 'mock_keyboard.lock')
    start_key = args.start_key
    stop_key = args.stop_key
    abort_key = args.abort_key
    if args.daemon:
        if os.path.exists(_lock_file):
            print("listener is already running.")
            sys.exit(0)
        start_daemon(_lock_file, start_key=start_key, stop_key=stop_key, abort_key=abort_key)
    elif args.stop:
        stop_listening(_lock_file)
    elif args.status:
        status(_lock_file)
        sys.exit(0)
    else:
        if os.path.exists(_lock_file):
            print("listener is already running.")
            sys.exit(0)
        start(_lock_file, start_key=start_key, stop_key=stop_key, abort_key=abort_key)
        sys.exit(0)


if __name__ == "__main__":
    main()
