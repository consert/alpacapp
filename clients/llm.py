"""Simple HTTP client."""
# pylint: disable=too-many-try-statements
import argparse
import json
import os
import sys
import time
from typing import Any, Dict
from urllib.error import HTTPError, URLError
from urllib.request import Request, urlopen

HERE = os.path.dirname(__file__)

try:
    from rich.console import Console

    # pylint: disable=redefined-builtin
    print = Console().print
except ImportError:
    pass


def cli() -> argparse.ArgumentParser:
    """Get the command line interface."""
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", "-H", default="0.0.0.0", help="Server host")
    parser.add_argument("--port", "-p", default=8000, help="Server port")
    parser.add_argument(
        "--model", "-M", default="falcon", help="Model name", choices=["falcon", "alpaca"]
    )
    parser.add_argument("--config", "-C", action="store_true", help="Get the config")
    parser.add_argument("--clear", "-c", action="store_true", help="Clear the history")
    parser.add_argument("--message", "-m", default="", help="Message to send")
    parser.add_argument("--speak", "-s", action="store_true", help="Speak the response")

    return parser


def main() -> None:
    """Run the main program."""
    args = cli().parse_args()
    url = f"http://{args.host}:{args.port}/v2/models/{args.model}"
    headers = {}
    if args.config is True:
        url += "/config"
        data = None
    else:
        url += "/infer"
        if not args.message:
            raise ValueError("No message to send")
        headers = {"Content-Type": "application/json"}
        data_dict: Dict[str, Any] = {
            "inputs": [
                {"name": "INPUT0", "datatype": "BYTES", "shape": [1, 1], "data": [args.message]}
            ]
        }
        if args.clear:
            data_dict["parameters"] = {"clear_history": "true"}
        data = json.dumps(data_dict).encode("utf-8")
    request = Request(url, headers=headers, data=data)
    try:
        start = time.perf_counter()
        with urlopen(request, timeout=600) as response:  # nosec
            response = response.read().decode("utf-8")
            request_time = time.perf_counter() - start
            response_dict = json.loads(response)
            if args.speak:
                response = response_dict["outputs"][0]["data"]
                if response:
                    cmd = f"{sys.executable} {os.path.join(HERE, 'tts.py')} {response}"
                    os.system(cmd)  # nosec
            print(response_dict)
            print(f'\nElapsed time: {request_time} s\n')
    except HTTPError as error:
        print(error.read())
    except URLError as error:
        print(error.reason)
    except TimeoutError:
        print("Request timed out")


if __name__ == "__main__":
    main()
