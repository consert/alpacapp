"""Speech-to-text client example."""

import argparse
import os
import signal as sig
import socket
import sys
from pathlib import Path
from types import FrameType
from typing import Optional

import riva.client
import riva.client.audio_io
from riva.client.argparse_utils import add_asr_config_argparse_parameters

HERE = Path(__file__).parent


def parse_args() -> argparse.Namespace:
    """Parse command line arguments."""
    default_device_info = riva.client.audio_io.get_default_input_device_info()
    default_device_index = None if default_device_info is None else default_device_info['index']
    parser = argparse.ArgumentParser(
        description="Streaming transcription from microphone via Riva AI Services",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--input-device",
        type=int,
        default=default_device_index,
        help="An input audio device to use.",
    )
    parser.add_argument(
        "--list-devices", action="store_true", help="List input audio device indices."
    )
    parser = add_asr_config_argparse_parameters(parser, profanity_filter=True)
    parser.add_argument("--riva-host", default="localhost", help="GRPC server host.")
    parser.add_argument("--riva-port", type=int, default=50051, help="GRPC server port.")
    parser.add_argument("--ssl-cert", help="Path to SSL client certificates file.")
    parser.add_argument(
        "--use-ssl",
        action='store_true',
        help="Boolean to control if SSL/TLS encryption should be used.",
    )
    parser.add_argument(
        "--sample-rate-hz",
        type=int,
        help="A number of frames per second in audio streamed from a microphone.",
        default=16000,
    )
    parser.add_argument(
        "--file-streaming-chunk",
        type=int,
        default=1600,
        help="A maximum number of frames in a audio chunk sent to server.",
    )
    parser.add_argument(
        '--tcp-host',
        type=str,
        default=None,
        help='The host to connect to.',
    )
    parser.add_argument(
        '--tcp-port',
        type=int,
        default=5009,
        help='The port to connect to.',
    )
    parser.add_argument(
        '--llm-host',
        type=str,
        default=None,
        help='The llm host to connect to.',
    )
    parser.add_argument(
        '--llm-port',
        type=int,
        default=30000,
        help='The llm port to connect to.',
    )
    parser.add_argument(
        '--llm-model',
        type=str,
        choices=['falcon', 'alpaca'],
        default='falcon',
        help='The llm model to use.',
    )
    parser.add_argument(
        '--to-speech',
        action='store_true',
        help='Speak the response from alpaca.',
    )
    args = parser.parse_args()
    return args


def connect(host: str, port: int) -> socket.socket:
    """Connect to the server."""
    addr = (host, port)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock.connect(addr)
    except ConnectionRefusedError:
        print("Connection refused. Is the server running?")
        os.system(f'{sys.executable} {HERE / "keyboard.py"} --stop')  # nosec
        sys.exit(1)
    return sock


# pylint: disable=unused-argument
def signal_handler(signal: int, frame: Optional[FrameType]) -> None:  # noqa: D103
    """Handle keyboard interrupt."""
    print('Got keyboard interrupt. Exiting...')
    os.system(f'{sys.executable} {HERE / "keyboard.py"} --stop')  # nosec
    sys.exit(0)


def send(client: socket.socket, msg: str, header: int = 128, format_: str = "utf-8") -> None:
    """Send a message to the tcp server."""
    print("Sending...")
    message = msg.encode(format_)
    msg_length = len(message)
    send_length = str(msg_length).encode(format_)
    send_length += b" " * (header - len(send_length))
    client.send(send_length)
    client.send(message)
    print("Sent!")


def send_to_llm(msg: str, model: str, host: Optional[str], port: int, speak: bool) -> None:
    """Send a message to the llm server."""
    if host is not None:
        cmd = f'{sys.executable} {HERE / "llm.py"} --host {host} --port {port} --model {model}'
        if speak:
            cmd += ' --speak'
        cmd += f' --message "{msg}"'
        os.system(cmd)  # nosec


def should_send() -> bool:
    """Check if a message should be sent."""
    return Path(HERE / 'keyboard.lock').exists()


# pylint: disable=too-many-arguments,too-many-locals
def riva_stream(
    sample_rate_hz: int,
    file_streaming_chunk: int,
    asr_service: riva.client.ASRService,
    config: riva.client.StreamingRecognitionConfig,
    llm_port: int,
    llm_model: str,
    llm_host: Optional[str],
    speak_response: bool,
    input_device: Optional[int],
    client: Optional[socket.socket],
) -> None:
    """Stream audio from microphone to Riva and print the transcript."""
    with riva.client.audio_io.MicrophoneStream(
        sample_rate_hz,
        file_streaming_chunk,
        device=input_device,
    ) as audio_chunk_iterator:
        responses = asr_service.streaming_response_generator(
            audio_chunks=audio_chunk_iterator, streaming_config=config
        )
        for response in responses:
            if not response.results:
                continue
            for result in response.results:
                if not result.alternatives:
                    continue
                if result.is_final:
                    transcript = result.alternatives[0].transcript.strip()
                    send_it = should_send()
                    if send_it and transcript:
                        print(f"send: {transcript}\n")
                        if client is not None:
                            send(client, transcript)
                        if llm_host is not None:
                            send_to_llm(
                                msg=transcript,
                                model=llm_model,
                                host=llm_host,
                                port=llm_port,
                                speak=speak_response,
                            )


def main() -> None:
    """Run the main function."""
    args = parse_args()
    if args.list_devices:
        riva.client.audio_io.list_input_devices()
        return
    client: Optional[socket.socket] = None
    if args.tcp_host is not None:
        client = connect(args.tcp_host, args.tcp_port)
    riva_socket = f'{args.riva_host}:{args.riva_port}'
    auth = riva.client.Auth(args.ssl_cert, args.use_ssl, riva_socket)
    asr_service = riva.client.ASRService(auth)
    config = riva.client.StreamingRecognitionConfig(
        config=riva.client.RecognitionConfig(
            encoding=riva.client.AudioEncoding.LINEAR_PCM,
            language_code=args.language_code,
            max_alternatives=1,
            profanity_filter=args.profanity_filter,
            enable_automatic_punctuation=args.automatic_punctuation,
            verbatim_transcripts=not args.no_verbatim_transcripts,
            sample_rate_hertz=args.sample_rate_hz,
            audio_channel_count=1,
        ),
        interim_results=True,
    )
    riva.client.add_word_boosting_to_config(config, args.boosted_lm_words, args.boosted_lm_score)
    kwargs = {
        'sample_rate_hz': args.sample_rate_hz,
        'file_streaming_chunk': args.file_streaming_chunk,
        'asr_service': asr_service,
        'config': config,
        'llm_port': args.llm_port,
        'llm_host': args.llm_host,
        'llm_model': args.llm_model,
        'input_device': args.input_device,
        'client': client,
        'speak_response': args.to_speech,
    }
    riva_stream(**kwargs)


if __name__ == '__main__':
    if not Path(HERE / 'keyboard.py').exists():
        print("keyboard.py not found")
        sys.exit(1)
    os.system(f'{sys.executable} {HERE / "keyboard.py"} --stop')  # nosec
    os.system(f'{sys.executable} {HERE / "keyboard.py"} --daemon')  # nosec
    sig.signal(sig.SIGINT, signal_handler)
    main()
