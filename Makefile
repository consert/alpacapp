.PHONY: requirements
requirements:
	sh scripts/requirements.sh

.PHONY: format
format:
	isort .
	autoflake --remove-all-unused-imports --remove-unused-variables --in-place .
	black --config pyproject.toml .

.PHONY: lint
lint:
	isort --check-only .
	black --config pyproject.toml --check .
	mypy --config pyproject.toml .
	flake8 --config=.flake8
	pylint --rcfile=pyproject.toml --recursive y --output-format=text app/ clients/
	pydocstyle --config pyproject.toml .
	bandit -c pyproject.toml -r .

.PHONY: clean
clean:
	rm -rf `find . -name __pycache__`
	rm -f `find . -type f -name '*.py[co]' `
	rm -f `find . -type f -name '*~' `
	rm -f `find . -type f -name '.*~' `
	rm -rf .cache
	rm -rf .mypy_cache

.PHONY: init
init: requirements
	pip install -r requirements/requirements.txt -r requirements/requirements_dev.txt
	make clean && make format && make lint
