---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Release.Name }}-deployment
  namespace: {{ .Values.namespace }}
spec:
  selector:
    matchLabels:
      app: {{ template "selector.name" . }}
  replicas: {{ .Values.replicaCount }}
  template:
    metadata:
      labels:
        app: {{ .Release.Name }}
    spec:
      restartPolicy: {{ .Values.deployment.restartPolicy }}
      containers:
      - name: {{ .Release.Name }}
        imagePullPolicy: {{ .Values.deployment.pullPolicy }}
        image: "{{ .Values.deployment.image.repository }}:{{ .Values.deployment.image.tag | default .Chart.AppVersion }}"
        ports:
          - containerPort: 8000
            name: http
          - containerPort: 8001
            name: grpc
          - containerPort: 8002
            name: metrics
        envFrom:
          - configMapRef:
              name: {{ .Release.Name }}-configmap
          - secretRef:
              name: {{ .Release.Name }}-secrets
        livenessProbe:
          initialDelaySeconds: {{ .Values.deployment.initialDelaySeconds }}
          periodSeconds: {{ .Values.deployment.periodSeconds }}
          httpGet:
            path: /v2/health/live
            port: http
        readinessProbe:
          initialDelaySeconds: {{ .Values.deployment.initialDelaySeconds }}
          periodSeconds: {{ .Values.deployment.periodSeconds }}
          httpGet:
            path: /v2/health/ready
            port: http
        resources:
          requests: {{ if .Values.env.USE_GPU }}
            nvidia.com/gpu: "{{ .Values.deployment.resources.numOfGPUs }}" {{ else }} {} {{ end }}
          limits:
            memory: "{{ .Values.deployment.resources.memory }}"
            cpu: "{{ .Values.deployment.resources.cpu }}" {{ if .Values.env.USE_GPU }}
            nvidia.com/gpu: "{{ .Values.deployment.resources.numOfGPUs }}" {{ end }}
        volumeMounts:
          - name: {{ .Release.Name }}-data
            mountPath: /home/user/data
          - name: shared-memory
            mountPath: /dev/shm
      initContainers:
      - name: {{ .Release.Name }}-init
        image: "{{ .Values.deployment.image.repository }}:{{ .Values.deployment.image.tag | default .Chart.AppVersion }}"
        imagePullPolicy: {{ .Values.pullPolicy }}
        command: [ "sh", "/home/user/scripts/init.sh" ]
        volumeMounts:
          - name: {{ .Release.Name }}-data
            mountPath: /home/user/data
          - name: shared-memory
            mountPath: /dev/shm
        securityContext:
          runAsUser: 0
          privileged: true
      securityContext:
        runAsUser: 1000
        runAsGroup: 1000
        fsGroup: 1000
      volumes:
        - name: shared-memory
          emptyDir:
            medium: Memory
        - name: {{ .Release.Name }}-data
          persistentVolumeClaim:
            claimName: {{ .Release.Name }}-pvc
