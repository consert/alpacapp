#!/bin/env sh
# Dummy script to create a user with sudo access
# (if logging in the first time as root [using an ssh key] in a new VM and no other user exists)
set -e
_ROOT_DIR="$(dirname "$(dirname "$(readlink -f "$0")")")"
if [ "${_ROOT_DIR}" = "." ]; then
    _ROOT_DIR="$(pwd)"
fi
_dirname="$(basename "${_ROOT_DIR}")"
adduser --disabled-password --gecos '' --shell /bin/bash user 2>/dev/null || true
echo "user ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/90-user
usermod -aG docker user 2>/dev/null || true
if [ -f /root/.ssh/authorized_keys ]; then
    mkdir -p /home/user/.ssh
    cp /root/.ssh/authorized_keys /home/user/.ssh/authorized_keys
fi
cp -rp "${_ROOT_DIR}" "/home/user/${_dirname}" 2>/dev/null || true
chown -R user:user /home/user/ 2>/dev/null || true
echo "You should now be able to login as \`user\`"
# exit
