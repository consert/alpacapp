#!/bin/env sh
#shellcheck disable=SC1091
_HERE="$(dirname "$(readlink -f "$0")")"
cd "${_HERE}" || exit 1
if [ -f  ./.env ]; then
    . ./.env
elif [ -f ./.env.template ]; then
    cp .env.template .env
    . ./.env
fi
me="$(whoami)"
DOCKER_CONTAINER_NAME=${DOCKER_CONTAINER_NAME:-alpacapp}
systemd_dir="/home/${me}/.config/systemd/user"
mkdir -p "${systemd_dir}"
cat <<EOF > "${systemd_dir}/alpacapp.service"

[Unit]
Description=alpacapp service

[Service]
Type=simple
ExecStart=/bin/sh ${_HERE}/service.sh --start
ExecStop=/bin/sh ${_HERE}/service.sh --stop
Restart=always

[Install]
WantedBy=default.target
EOF

systemctl --user daemon-reload

_stop_docker() {
    docker stop "${DOCKER_CONTAINER_NAME}" > /dev/null || true
}

start() {
    _stop_docker
    sh "${_HERE}/docker_run.sh"
}

stop() {
    _stop_docker
}

restart () {
    start
    stop
}

if [ "${1}" = "--start" ]; then
    start
elif [ "${1}" = "--stop" ]; then
    stop
elif [ "${1}" = "--restart" ]; then
    restart
else
    systemctl --user start alpacapp.service
    echo "use: \`systemctl --user start|stop|restart|status alpacapp \`"
    echo "use: \`journalctl --user -xefu alpacapp\` to see (f for follow) logs"
fi
