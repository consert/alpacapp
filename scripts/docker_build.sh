#!/bin/env sh

set -e

_ROOT_DIR="$(dirname "$(dirname "$(readlink -f "$0")")")"
if [ "${_ROOT_DIR}" = "." ]; then
    _ROOT_DIR="$(pwd)"
fi
cd "${_ROOT_DIR}" || exit 1

if [ -f ./.env ]; then
    #shellcheck disable=SC1091
    . ./.env
fi

DOCKER_IMAGE_NAME=${DOCKER_IMAGE_NAME:-alpacapp}
DOCKER_IMAGE_VERSION=${DOCKER_IMAGE_VERSION:-0.0.1}

BASE_IMAGE=nvcr.io/nvidia/cuda:12.1.1-cudnn8-devel-ubuntu20.04

if [ "${1}" = "cpu" ] || [ "${1}" = "--cpu" ]; then
    shift
    BASE_IMAGE="python:3.8-slim"
    DOCKER_IMAGE_TAG="${DOCKER_IMAGE_TAG:-cpu}"
    if [ "${DOCKER_IMAGE_TAG}" = "cuda" ] || [ "${DOCKER_IMAGE_TAG}" = "gpu" ]; then
        # not changed in .env?
        DOCKER_IMAGE_TAG=cpu
    fi
else
    DOCKER_IMAGE_TAG="${DOCKER_IMAGE_TAG:-cuda}"
fi
DOCKER_TAG="${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION}_${DOCKER_IMAGE_TAG}"

# for verbose output (buildkit) and force build: (--no-cache)
# BUILDKIT_PROGRESS=plain docker build --no-cache --build-arg="BASE_IMAGE=${BASE_IMAGE}" -t "${DOCKER_TAG}" .
docker build --build-arg="BASE_IMAGE=${BASE_IMAGE}" -t "${DOCKER_TAG}" .

if [ "${1}" = "--push" ] || [ "${1}" = "push" ]; then
    docker push "${DOCKER_TAG}"
fi
