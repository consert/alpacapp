#!/bin/env bash

# Make sure we have enough VRAM (at least 12Gb)
# https://docs.nvidia.com/deeplearning/riva/user-guide/docs/support-matrix.html#support-matrix

set -e

NAMESPACE="${NAMESPACE:-alpacapp}"
NGC_CLI_API_KEY="${NGC_CLI_API_KEY:-${NVRC_API_KEY:-}}"

if [ -z "${NGC_CLI_API_KEY}" ]; then
    echo "NGC_CLI_API_KEY is not set"
    exit 1
fi

helm delete riva-api --namespace "${NAMESPACE}" 2>/dev/null || true

VERSION_TAG="${RIVA_VERSION:-${VERSION:-2.11.0}}"
NGC_CREDENTIALS="$(echo -n "${NGC_CLI_API_KEY}" | base64 -w0)"
MODEL_DEPLOY_KEY="$(echo -n plato-asr | base64 -w0)"

if [ ! -f "riva-api-${VERSION_TAG}.tgz" ]; then
    helm fetch "https://helm.ngc.nvidia.com/nvidia/riva/charts/riva-api-${VERSION_TAG}.tgz" --username=\$oauthtoken --password="${NGC_CLI_API_KEY}"
fi
if [ -d riva-api ]; then
    rm -rf riva-api
fi
tar -xvzf "riva-api-${VERSION_TAG}.tgz"
if [ -f riva.patch ]; then
    patch riva-api/templates/deployment.yaml < riva.patch
fi

helm install riva-api riva-api/ \
    --set ngcCredentials.password="${NGC_CREDENTIALS}" \
    --set modelRepoGenerator.modelDeployKey="${MODEL_DEPLOY_KEY}" \
    --set riva.speechServices.asr=true \
    --set riva.speechServices.nlp=false \
    --set riva.speechServices.tts=false \
    --set riva.speechServices.nmt=false \
    --namespace "${NAMESPACE}" \
    --create-namespace

if [ -f "riva-api-${VERSION_TAG}.tgz" ]; then
    rm "riva-api-${VERSION_TAG}.tgz"
fi
if [ -d riva-api ]; then
    rm -rf riva-api
fi
