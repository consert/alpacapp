#!/bin/env sh

set -e

_sudo() {
    if [ "$(id -u)" -ne 0 ]; then
        sudo "$@"
    else
        "$@"
    fi
}
have_k9s() {
    command -v k9s >/dev/null 2>&1
}

if ! have_k9s; then
    rm -rf .tmp && mkdir -p .tmp && cd .tmp
    k9s_version="v$(curl -w '%{redirect_url}' -o /dev/null -s https://github.com/derailed/k9s/releases/latest | sed 's:\(.*/tag/v\)\(.*\):\2:')"
    curl -LO "https://github.com/derailed/k9s/releases/download/$k9s_version/k9s_Linux_amd64.tar.gz"
    tar xzvf k9s_Linux_amd64.tar.gz && chmod +x k9s
    _sudo mv k9s /usr/local/bin
    cd .. && rm -rf .tmp
fi
