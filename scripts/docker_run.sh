#!/bin/env sh
#shellcheck disable=SC1091

set -e

_ROOT_DIR="$(dirname "$(dirname "$(readlink -f "$0")")")"
if [ "${_ROOT_DIR}" = "." ]; then
    _ROOT_DIR="$(pwd)"
fi
cd "${_ROOT_DIR}" || exit 1

if [ -f ./.env ]; then
    . ./.env
elif [ -f ./.env.template ]; then
    cp .env.template .env
    . ./.env
else
    touch .env
fi

if [ -f "${_ROOT_DIR}/scripts/docker_build.sh" ]; then
    if [ "${1}" = "cpu" ] || [ "${1}" = "--cpu" ]; then
        sh "${_ROOT_DIR}/scripts/docker_build.sh" --cpu
    else
        sh "${_ROOT_DIR}/scripts/docker_build.sh"
    fi
fi

DOCKER_IMAGE_NAME=${DOCKER_IMAGE_NAME:-alpaca}
DOCKER_IMAGE_VERSION=${DOCKER_IMAGE_VERSION:-0.0.1}

if [ "${1}" = "cpu" ] || [ "${1}" = "--cpu" ]; then
    shift
    DOCKER_IMAGE_TAG="${DOCKER_IMAGE_TAG:-cpu}"
    if [ "${DOCKER_IMAGE_TAG}" = "cuda" ] || [ "${DOCKER_IMAGE_TAG}" = "gpu" ]; then
        # not changed in .env?
        DOCKER_IMAGE_TAG=cpu
    fi
else
    DOCKER_IMAGE_TAG="${DOCKER_IMAGE_TAG:-cuda}"
fi
DOCKER_IMAGE="${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION}_${DOCKER_IMAGE_TAG}"

HTTP_PORT=${HTTP_PORT:-8000}
GRPC_PORT=${GRPC_PORT:-8001}
METRICS_PORT=${METRICS_PORT:-8002}
DOCKER_CONTAINER_NAME=${DOCKER_CONTAINER_NAME:-alpacapp}


if [ "${1}" = "-it" ]; then
    docker run --gpus all --rm -it \
        -p "${HTTP_PORT}:${HTTP_PORT}" -p "${GRPC_PORT}:${GRPC_PORT}" -p "${METRICS_PORT}:${METRICS_PORT}" \
        --shm-size=1024m --ipc=host \
        -e HTTP_PORT="${HTTP_PORT}" \
        -e GRPC_PORT="${GRPC_PORT}" \
        -e METRICS_PORT="${METRICS_PORT}" \
        -e NVIDIA_VISIBLE_DEVICES=all \
        -v "$(pwd)/.env":/home/user/app/.env \
        -v "$(pwd)/data":/home/user/data \
        --name "${DOCKER_CONTAINER_NAME}" \
        "${DOCKER_IMAGE}" bash
else
    docker run --gpus all --rm \
        -p "${HTTP_PORT}:${HTTP_PORT}" -p "${GRPC_PORT}:${GRPC_PORT}" -p "${METRICS_PORT}:${METRICS_PORT}" \
        --shm-size=1024m --ipc=host \
        -e HTTP_PORT="${HTTP_PORT}" \
        -e GRPC_PORT="${GRPC_PORT}" \
        -e METRICS_PORT="${METRICS_PORT}" \
        -e NVIDIA_VISIBLE_DEVICES=all \
        -v "$(pwd)/.env":/home/user/app/.env \
        -v "$(pwd)/data":/home/user/data \
        --name "${DOCKER_CONTAINER_NAME}" \
        "${DOCKER_IMAGE}"
fi
