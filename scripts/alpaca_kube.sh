#!/bin/sh

# Only helm for now.

set -e
_ROOT_DIR="$(dirname "$(dirname "$(readlink -f "$0")")")"
if [ "${_ROOT_DIR}" = "." ]; then
    _ROOT_DIR="$(pwd)"
fi
have_kubectl() {
    command -v kubectl >/dev/null 2>&1
}

have_minikube() {
    command -v minikube >/dev/null 2>&1
}

have_helm() {
    command -v helm >/dev/null 2>&1
}

if ! have_kubectl; then
    if not have_minikube; then
        echo "kubectl is not installed"
        exit 1
    fi
    # else we can alias kubeclt to `minikube kubectl`
fi

manifests="${_ROOT_DIR}/kubernetes"
if [ ! -d "${manifests}" ]; then
    echo "manifests directory not found"
    exit 1
fi

if ! have_helm; then
    echo "helm is not installed"
    exit 1
fi

cd "${manifests}/helm" || exit 1

helm template alpacapp . \
    --set deployment.image.repository=docker.io/your/repository \
    --set deployment.image.tag=0.0.1_cuda \
    --set env.MODEL_FILENAME=model.bin \
    --set deployment.pullPolicy=Always > "${manifests}/alpacapp.yaml"

echo "Generated ${manifests}/alpacapp.yaml"
echo "Use \`kubectl apply -f ${manifests}/alpacapp.yaml\` to deploy the app."
echo "If you are using minikube, you might need to use:"
echo "\`minikube kubectl apply -- f ${manifests}/alpacapp.yaml \` instead."
