#!/bin/env bash
# initialize the first time as root (to avoid permissions issues for the /home/user/data volume)

set -e

if [ "$(id -u)" = "0" ]; then
    mkdir -p /home/user/data && chown -R user:user /home/user/data
    if [ ! -f /home/user/data/model.bin ] && [ -f /home/user/scripts/llama_cpp.sh ] ; then
        # convert and quantize for llama.cpp (generate ./data/model{,-q4_0,-q8_0}.bin)
        su - user -c "USE_LLAMA_CPP=true bash /home/user/scripts/llama_cpp.sh"
    fi
    if [ -f /home/user/app/models/impl/falcon.py ]; then
        # donload from huggingface the falcon model
        echo "Initializing Falcon..."
        # we might get an error in the initContainer :( but it's ok kind of, it will be retried later (on startup)
        su - user -c "HF_HOME=/home/user/data USE_FALCON=true python3 /home/user/app/models/impl/falcon.py 2>/dev/null || :"
        echo "Done."
    else
        echo "Could not find /home/user/app/models/impl/falcon.py"
    fi
    if [ -f /home/user/app/models/impl/alpaca.py ]; then
        # donload from huggingface the alpaca model
        echo "Initializing Alpaca..."
        # we might get an error initContainer :( but it's ok kind of, it will be retried later (on startup)
        su - user -c "HF_HOME=/home/user/data USE_ALPACA=true python3 /home/user/app/models/impl/alpaca.py 2>/dev/null || :"
        echo "Done."
    else
        echo "Could not find /home/user/app/models/impl/alpaca.py"
    fi
    chown -R user:user /home/user
else
    ls -lah /home/user/data
    if [ ! -f /home/user/data/model.bin ] && [ -f /home/user/scripts/llama_cpp.sh ] ; then
        USE_LLAMA_CPP=true bash /home/user/scripts/llama_cpp.sh
    fi
    if [ ! -d /home/user/data/hub ]; then
        # should not be needed, but just in case
        if [ -f /home/user/app/models/impl/falcon.py ]; then
            echo "Initializing Falcon..."
            HF_HOME=/home/user/data USE_FALCON=true python3 /home/user/app/models/impl/falcon.py
            echo "Done."
        else
            echo "Could not find /home/user/app/models/impl/falcon.py"
        fi
        if [ -f /home/user/app/models/impl/alpaca.py ]; then
            echo "Initializing Alpaca..."
            HF_HOME=/home/user/data USE_ALPACA=true python3 /home/user/app/models/impl/alpaca.py
            echo "Done."
        else
            echo "Could not find /home/user/app/models/impl/alpaca.py"
        fi
    fi
fi
