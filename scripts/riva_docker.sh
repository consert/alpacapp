#!/bin/env sh
# shellcheck disable=SC2016,SC1003
set -e

_ROOT_DIR="$(dirname "$(dirname "$(readlink -f "$0")")")"
if [ "${_ROOT_DIR}" = "." ]; then
    _ROOT_DIR="$(pwd)"
fi
cd "${_ROOT_DIR}" || exit 1

if [ -f ./.env ]; then
    #shellcheck disable=SC1091
    . ./.env
fi
export CUDA_MODULE_LOADING=LAZY

cd "${HOME}" || exit 1

have_ngccli() {
    command -v ngc >/dev/null 2>&1
}

if ! have_ngccli; then
    echo "Please install ngc cli first."
    echo "See https://ngc.nvidia.com/setup/installers/cli"
    echo "Also make sure you run \`ngc config set\` to set your API key before running this script."
    exit 1
fi

ngc registry resource download-version nvidia/riva/riva_quickstart:2.11.0

cd riva_quickstart_v2.11.0 || exit 1

# only enable the asr service
sed -i 's/service_enabled_asr=false/service_enabled_asr=true/g' config.sh
sed -i 's/service_enabled_nlp=true/service_enabled_nlp=false/g' config.sh
sed -i 's/service_enabled_tts=true/service_enabled_tts=false/g' config.sh
sed -i 's/service_enabled_nmt=true/service_enabled_nmt=false/g' config.sh


# add the CUDA_MODULE_LOADING=LAZY env for the warning:
# CUDA lazy loading is not enabled. 
# Enabling it can significantly reduce device memory usage.
# See `CUDA_MODULE_LOADING` in
# https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#env-vars
sed -i '/NGC_CLI_ORG=nvidia/a\\t\t-e "CUDA_MODULE_LOADING=LAZY" \\' riva_init.sh
sed -i '/RIVA_EULA=\$RIVA_EULA/a\\t-e "CUDA_MODULE_LOADING=LAZY" \\' riva_start.sh

bash riva_init.sh

echo "Use \`bash ${HOME}/riva_quickstart_v2.11.0/riva_start.sh\` to start the Riva services"
echo "Use \`bash ${HOME}/riva_quickstart_v2.11.0/riva_stop.sh\` to stop the Riva services."
