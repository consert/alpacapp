#!/usr/bin/env sh
set -e

_HERE="$(dirname "$(readlink -f "$0")")"
_ROOT_DIR="$(dirname "${_HERE}")"
if [ -f "${_ROOT_DIR}/.venv/bin/activate" ]; then
  if [ ! "$(which python)" = "${_ROOT_DIR}/.venv/bin/python" ]; then
      echo "Activating virtual environment..."
      # shellcheck disable=SC1090,SC1091
      . "${_ROOT_DIR}/.venv/bin/activate"
  fi
fi
cd "${_ROOT_DIR}" || exit 1
mkdir -p requirements
pip install toml

cat << EOF > tmp.py
import toml
import sys

file = sys.argv[1]
section = sys.argv[2]
subsection = sys.argv[3]
with open(file, "r", encoding="utf-8") as f:
    data = toml.load(f)
    if section in data["project"]:
        for _subsection in data["project"][section]:
            if not subsection:
                print(_subsection)
            elif subsection == _subsection:
                for _package in data["project"][section][_subsection]:
                    print(_package)
            else:
                continue
EOF

python3 tmp.py pyproject.toml dependencies "" | sort > requirements/requirements.txt
python3 tmp.py pyproject.toml optional-dependencies dev | sort > requirements/requirements_dev.txt
echo "Done. Generated requirements files:"
ls -l requirements/requirements*.txt
rm tmp.py

for file in requirements/requirements*.txt; do
  # replace {git = "...", branch = "..."} with git+[https|ssh]//...@...
  sed -i 's/\(.*\){git\(.*\)=\(.*\)"\(.*\)",\(.*\)branch\(.*\)=\(.*\)"\(.*\)"}/git+\4@\8/g' "${file}"
  # re-sort
  sort < "${file}" > "${file}.tmp" && mv "${file}.tmp" "${file}"
done
