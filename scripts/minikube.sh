#!/bin/sh
# shellcheck disable=SC2046,SC2086

set -e

if [ ! -x /usr/bin/nvidia-container-runtime ]; then
    echo "nvidia-container-runtime is not installed"
    exit 1
fi

if ! systemctl is-active --quiet containerd; then
    echo "containerd is not running"
    exit 1
fi

have_conntrack() {
    command -v conntrack >/dev/null 2>&1
}

install_conntrack() {
    # apt for debian based, dnf for rpm based
    if command -v apt >/dev/null 2>&1; then
        _sudo apt install -y conntrack
    elif command -v dnf >/dev/null 2>&1; then
        _sudo dnf install -y conntrack-tools
    else
        echo "no package manager found"
        exit 1
    fi
}

_sudo() {
    if [ "$(id -u)" -ne 0 ]; then
        sudo "$@"
    else
        "$@"
    fi
}

containerd_config() {
    _sudo mkdir -p /etc/containerd
    cat <<EOF > etc_containerd_config.toml
version = 2
[plugins]
  [plugins."io.containerd.grpc.v1.cri"]
    [plugins."io.containerd.grpc.v1.cri".containerd]
      default_runtime_name = "nvidia"

      [plugins."io.containerd.grpc.v1.cri".containerd.runtimes]
        [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.nvidia]
          privileged_without_host_devices = false
          runtime_engine = ""
          runtime_root = ""
          runtime_type = "io.containerd.runc.v2"
          [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.nvidia.options]
            BinaryName = "/usr/bin/nvidia-container-runtime"
EOF
    _sudo mv etc_containerd_config.toml /etc/containerd/config.toml
}

have_minikube() {
    command -v minikube >/dev/null 2>&1
}

install_minikube () {
    r=https://api.github.com/repos/kubernetes/minikube/releases
    curl -LO $(curl -s $r | grep -o 'http.*download/v.*beta.*/minikube-linux-amd64' | head -n1)
    chmod +x minikube-linux-amd64 && _sudo mv minikube-linux-amd64 /usr/local/bin/minikube
}

have_crictl() {
    command -v crictl >/dev/null 2>&1
}

install_crictl() {
    CRICTL_VERSION="v${CRICTL_VERSION:-1.26.0}"
    curl -LO https://github.com/kubernetes-sigs/cri-tools/releases/download/$CRICTL_VERSION/crictl-$CRICTL_VERSION-linux-amd64.tar.gz
    tar xzvf "crictl-${CRICTL_VERSION}-linux-amd64.tar.gz"
    _sudo mv crictl /usr/local/bin && rm "crictl-${CRICTL_VERSION}-linux-amd64.tar.gz"
}

device_plugin () {
    minikube kubectl delete -- -f https://raw.githubusercontent.com/NVIDIA/k8s-device-plugin/master/nvidia-device-plugin.yml > /dev/null 2>&1 || true
    minikube kubectl create -- -f https://raw.githubusercontent.com/NVIDIA/k8s-device-plugin/master/nvidia-device-plugin.yml > /dev/null 2>&1 || true
    # on the logs (k9s ftw), we should see sth like:
    # 2023/06/07 10:15:48 Retreiving plugins.                                                                                                                                                     │
    # 2023/06/07 10:15:48 Detected NVML platform: found NVML library                                                                                                                              │
    # 2023/06/07 10:15:48 Detected non-Tegra platform: /sys/devices/soc0/family file not found                                                                                                    │
    # 2023/06/07 10:15:48 Starting GRPC server for 'nvidia.com/gpu'                                                                                                                               │
    # 2023/06/07 10:15:48 Starting to serve 'nvidia.com/gpu' on /var/lib/kubelet/device-plugins/nvidia-gpu.sock                                                                                   │
    # 2023/06/07 10:15:48 Registered device plugin for 'nvidia.com/gpu' with Kubelet 
    #
}


start_minikube() {
    if ! minikube status > /dev/null 2>&1; then
        minikube start --driver=none --apiserver-ips 127.0.0.1 --apiserver-name localhost --network-plugin=cni --cni=calico --container-runtime=containerd
    fi
}

if ! have_conntrack; then
    install_conntrack
fi

if ! have_minikube; then
    install_minikube
fi

if ! have_crictl; then
    install_crictl
fi

containerd_config
_sudo systemctl restart containerd
start_minikube
device_plugin
_sudo cat /etc/containerd/config.toml
echo "re-run this script if needed"
