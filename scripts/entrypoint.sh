#!/bin/env bash

_ROOT_DIR="$(dirname "$(dirname "$(readlink -f "$0")")")"
if [ "${_ROOT_DIR}" = "." ]; then
    _ROOT_DIR="$(pwd)"
fi
if [ ! "$(id -u)" = "0" ] && [ -f "${_ROOT_DIR}/scripts/init.sh" ]; then
    bash "${_ROOT_DIR}/scripts/init.sh"
fi
NUMEXPR_MAX_THREADS="$(nproc)"
export NUMEXPR_MAX_THREADS="${NUMEXPR_MAX_THREADS}"
export HF_HOME="${_ROOT_DIR}/data"
export PYTHONUNBUFFERED=1

exec "$@"
