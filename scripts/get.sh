#!/bin/env sh

REPO="https://gitlab.com/consert/alpacapp"

cd "${HOME}" || exit 1

if [ -d alpacapp ]; then
    mv alpacapp alpacapp.bak
fi

git clone "${REPO}" alpacapp
cd alpacapp || exit 1
sh scripts/service.sh
