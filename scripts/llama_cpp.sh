#!/bin/env bash

set -e
_ROOT_DIR="$(dirname "$(dirname "$(readlink -f "$0")")")"
if [ "${_ROOT_DIR}" = "." ]; then
    _ROOT_DIR="$(pwd)"
fi

mkdir -p "${_ROOT_DIR}/data"

export HF_HOME="${_ROOT_DIR}/data"

if [ "${USE_LLAMA_CPP:-false}" = "true" ] && [ ! -f "${_ROOT_DIR}/data/model.bin" ]; then
    cd "${_ROOT_DIR}" || exit
    # we should already have git-lfs
    git clone https://huggingface.co/chainyo/alpaca-lora-7b
    git clone https://github.com/ggerganov/llama.cpp.git
    cd llama.cpp || exit
    python3 -m pip install sentencepiece transformers
    python3 convert.py "${_ROOT_DIR}/alpaca-lora-7b/" --outfile "${_ROOT_DIR}/data/model.bin"
    make -j
    ./quantize "${_ROOT_DIR}/data/model.bin" "${_ROOT_DIR}/data/model-q4_0.bin" q4_0
    ./quantize "${_ROOT_DIR}/data/model.bin" "${_ROOT_DIR}/data/model-q8_0.bin" q8_0
    cd "${_ROOT_DIR}"/ || exit
    rm -rf llama.cpp
    rm -rf alpaca-lora-7b
fi
